rm -rf News_Deli_Sys/bin
mkdir News_Deli_Sys/bin
find News_Deli_Sys/src -type f -name '*.java' -exec javac -d News_Deli_Sys/bin -cp News_Deli_Sys/lib/junit.jar {} +
if [ $? -ne 0 ]; then
	exit 1;
fi

cd News_Deli_Sys/bin
find ./ -type f -name '*.class' -exec jar cfe Newspaper_Delivery_System.jar Create_Cust {} +
if [ $? -ne 0 ]; then
  echo "Jar Not creating";
else
  echo "Jar creating";
fi 

find ./ -type f -printf "%f\n"