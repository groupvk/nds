package com.nds.GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;

import com.nds.Validate.ValidatePublicationEnter;
import com.nds.bean.Publication;
import com.nds.dao.impl.PublicationDaoImpl;
import com.nds.dbutil.ConnectionManager;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.ScrollPaneConstants;

/**
 * 
 * @author Huiyi He
 *
 */

public class PublicationManagement {

	private JFrame frmPublicationManagement;
	private JTextField pub_idText;
	private JTextField PnameText;
	private JTextField numOfStockText;
	private JTextField priceText;
	private PublicationDaoImpl ps = new PublicationDaoImpl();
	private ValidatePublicationEnter vp = new ValidatePublicationEnter();
	private JTable publicationTable;

	public JFrame getFrame() {
		return frmPublicationManagement;
	}

	/**
	 * Create the application.
	 */
	public PublicationManagement() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPublicationManagement = new JFrame();
		frmPublicationManagement.setTitle("Publication Management");
		frmPublicationManagement.setBounds(100, 100, 926, 617);
		frmPublicationManagement.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPublicationManagement.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("Pub_ID:");
		lblNewLabel.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		lblNewLabel.setBounds(56, 55, 72, 18);
		frmPublicationManagement.getContentPane().add(lblNewLabel);

		pub_idText = new JTextField();
		pub_idText.setBounds(139, 54, 154, 24);
		frmPublicationManagement.getContentPane().add(pub_idText);
		pub_idText.setColumns(10);

		JLabel lblName = new JLabel("  Name:");
		lblName.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		lblName.setBounds(56, 105, 72, 18);
		frmPublicationManagement.getContentPane().add(lblName);

		JLabel lblStock = new JLabel("   Stock:");
		lblStock.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		lblStock.setBounds(56, 155, 72, 18);
		frmPublicationManagement.getContentPane().add(lblStock);

		JLabel lblPrice = new JLabel("    Price:");
		lblPrice.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		lblPrice.setBounds(56, 205, 72, 18);
		frmPublicationManagement.getContentPane().add(lblPrice);

		PnameText = new JTextField();
		PnameText.setColumns(10);
		PnameText.setBounds(139, 104, 154, 24);
		frmPublicationManagement.getContentPane().add(PnameText);

		numOfStockText = new JTextField();
		numOfStockText.setColumns(10);
		numOfStockText.setBounds(139, 154, 154, 24);
		frmPublicationManagement.getContentPane().add(numOfStockText);

		priceText = new JTextField();
		priceText.setColumns(10);
		priceText.setBounds(139, 204, 154, 24);
		frmPublicationManagement.getContentPane().add(priceText);

		JLabel lblMessage = new JLabel("Message:");
		lblMessage.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 17));
		lblMessage.setBounds(58, 454, 94, 27);
		frmPublicationManagement.getContentPane().add(lblMessage);

		JTextArea messageText = new JTextArea();
		messageText.setBounds(164, 458, 702, 73);
		frmPublicationManagement.getContentPane().add(messageText);

		JButton Add = new JButton("ADD");
		Add.setFont(new Font("Calibri", Font.BOLD, 16));
		Add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean flag = true;
				
				messageText.setText("");
				
				int pub_id = Integer.parseInt(pub_idText.getText());
				String Pname = PnameText.getText();
				int numOfStock = Integer.parseInt(numOfStockText.getText());
				String price = priceText.getText();

				if(!vp.isPubNameValid(Pname)) {
					flag = false;
					messageText.setText("length of Pname should greater than 2 , lower than 30");
				}
				if(!vp.isPubPriceValid(price)) {
					flag = false;
					messageText.setText("price equal or should greater than 0.1,equal or lower than 1000");
				}
				if(!vp.isPubStockValid(numOfStock)) {
					flag = false;
					messageText.setText("numOfStock equal or should greater than 0,equal or lower than 10000");
				}
				if(!vp.isPubIDValid(pub_id)) {
					flag = false;
					messageText.setText("pub_id should equal or greater than 1,equal or lower than 5000");
				}
				
				if(flag) {

					Publication p = new Publication();
					p.setPub_id(pub_id);
					p.setPname(Pname);
					p.setNumOfStock(numOfStock);
					p.setPrice(price);
					ps.addPub(p);

					fillTable();
				}
				
			}
		});
		Add.setBounds(58, 265, 94, 29);
		frmPublicationManagement.getContentPane().add(Add);

		JButton delete = new JButton("DELETE");
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
//				int pub_id = Integer.parseInt(pub_idText.getText());
//				String Pname = PnameText.getText();

				int count = 0;
//				if (pub_idText.getText() != "") {
//					count = ps.delPubById(pub_id);
//				} else if (PnameText.getText() != null) {
//					count = ps.delPubByName(Pname);
//				}
				int flag1 = 0;
				if(!pub_idText.getText().equals("")) {
					flag1=1;
				}else if(!PnameText.getText().equals("")) {
					flag1=2;
				}
				
				if(flag1==1) {
					int pub_id = Integer.parseInt(pub_idText.getText());
					count = ps.delPubById(pub_id);
				}else if(flag1==2) {
					String Pname = PnameText.getText();
					count = ps.delPubByName(Pname);
				}
				
				if (count != 1) {
					messageText.setText("");
					messageText.setText("failed to delete");
				}else {
					messageText.setText("Successfully deleted");
				}
				fillTable();

			}
		});
		delete.setFont(new Font("Calibri", Font.BOLD, 16));
		delete.setBounds(199, 265, 94, 29);
		frmPublicationManagement.getContentPane().add(delete);

		JButton update = new JButton("UPDATE");
		update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int pub_id = Integer.parseInt(pub_idText.getText());
				String Pname = PnameText.getText();
				int numOfStock = Integer.parseInt(numOfStockText.getText());
				String price = priceText.getText();

				Publication p = new Publication();
				p.setPub_id(pub_id);
				p.setPname(Pname);
				p.setNumOfStock(numOfStock);
				p.setPrice(price);

				int count = ps.update(p);
				if (count != 1) {
					messageText.setText("");
					messageText.setText("failed to update");
				}else {
					messageText.setText("Update successfully");
				}
				fillTable();

			}
		});
		update.setFont(new Font("Calibri", Font.BOLD, 16));
		update.setBounds(56, 328, 94, 29);
		frmPublicationManagement.getContentPane().add(update);

		JButton UpdateStock = new JButton("UPDATE-STOCK");
		UpdateStock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int pub_id = Integer.parseInt(pub_idText.getText());
				int numOfStock = Integer.parseInt(numOfStockText.getText());
				
				int count =ps.updateStock(pub_id, numOfStock);
				if (count != 1) {
					messageText.setText("");
					messageText.setText("failed to update");
				}else {
					messageText.setText("Update successfully");
				}
				fillTable();
			}
		});
		UpdateStock.setFont(new Font("Calibri", Font.BOLD, 16));
		UpdateStock.setBounds(21, 386, 154, 29);
		frmPublicationManagement.getContentPane().add(UpdateStock);

		JButton find = new JButton("FIND");
		find.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				int pub_id = 0;
//				if(!pub_idText.equals(null)) {
//					pub_id = Integer.parseInt(pub_idText.getText());
//				}
//				
//				String Pname = PnameText.getText();
//				Publication p = null;
//				messageText.setText("");
//				if (pub_idText.getText() != "") {
//					p = ps.findById(pub_id);
//				} else if (!Pname.equals("")) {
//					p = ps.findByName(Pname);
//				}
//				if (p != null) {
//					messageText.setText(p.toString());
//				} else
//					messageText.setText("No record");
				
				Publication p = null;
				int flag = 0;
				if(!pub_idText.getText().equals("")) {
					flag=1;
				}else if(!PnameText.getText().equals("")) {
					flag=2;
				}
				
				if(flag==1) {
					int pub_id = Integer.parseInt(pub_idText.getText());
					p = ps.findById(pub_id);
				}else if(flag==2) {
					String Pname = PnameText.getText();
					p = ps.findByName(Pname);
				}
				
				
				if (p != null) {
					messageText.setText(p.toString());
				} else
					messageText.setText("No record");
			}
		});
		find.setFont(new Font("Calibri", Font.BOLD, 16));
		find.setBounds(199, 328, 94, 29);
		frmPublicationManagement.getContentPane().add(find);

		JButton btnClear = new JButton("CLEAR");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pub_idText.setText("");
				PnameText.setText("");
				numOfStockText.setText("");
				priceText.setText("");
				messageText.setText("");
			}
		});
		btnClear.setFont(new Font("Calibri", Font.BOLD, 16));
		btnClear.setBounds(199, 386, 94, 29);
		frmPublicationManagement.getContentPane().add(btnClear);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(342, 49, 516, 338);
		frmPublicationManagement.getContentPane().add(scrollPane);

		publicationTable = new JTable();
		publicationTable.setModel(
				new DefaultTableModel(new Object[][] {}, new String[] { "pub_id", "Pname", "numOfStock", "price" }) {
					boolean[] columnEditables = new boolean[] { false, false, false, false };

					public boolean isCellEditable(int row, int column) {
						return columnEditables[column];
					}
				});
		scrollPane.setViewportView(publicationTable);
		this.fillTable();
	}

	private void fillTable() {
		DefaultTableModel dtm = (DefaultTableModel) publicationTable.getModel();
		dtm.setRowCount(0);
		Vector vs = ps.finaAll();
		for (Object v : vs) {
			dtm.addRow((Vector) v);
		}

	}
}
