package com.nds.Validate;

public class ValidatePublicationEnter {
	public boolean isPubIDValid(int pub_id){
		if(pub_id<1 || pub_id>5000) {
			return false;
		}else
			return true;
	}
	public boolean isPubNameValid(String Pname) {
		if(Pname.length()<2 || Pname.length()>30) {
			return false;
		}else
			return true;
	}
	public  boolean isPubStockValid(int numOfStock) {
		if(numOfStock<0 || numOfStock>10000) {
			return false;
		}else
			return true;
	}
	public  boolean isPubPriceValid(String price) {
		double p = Double.valueOf(price.toString());
		if(p<0.1 || p>1000) {
			return false;
		}else
			return true;
	}

}
