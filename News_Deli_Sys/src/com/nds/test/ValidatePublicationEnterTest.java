package com.nds.test;



import com.nds.Validate.ValidatePublicationEnter;

import junit.framework.TestCase;

public class ValidatePublicationEnterTest extends TestCase {
	
	ValidatePublicationEnter vp = new ValidatePublicationEnter();
	
	public void testLowerValid001() {
		assertEquals(true, vp.isPubIDValid(1));
		assertEquals(true, vp.isPubNameValid("Ka"));
		assertEquals(true, vp.isPubPriceValid("0.1"));
		assertEquals(true, vp.isPubStockValid(0));
	}

	public void testIsLowerInValid() {
		assertEquals(false, vp.isPubIDValid(0));
		assertEquals(false, vp.isPubNameValid("K"));
		assertEquals(false, vp.isPubPriceValid("0"));
		assertEquals(false, vp.isPubStockValid(-1));
	}

	public void testIsLUpperValid() {
		assertEquals(true, vp.isPubIDValid(5000));
		assertEquals(true, vp.isPubNameValid("abcdefghijabcdefghijabcdefghij"));
		assertEquals(true, vp.isPubPriceValid("1000"));
		assertEquals(true, vp.isPubStockValid(10000));
	}

	public void testIsUpperInValid() {
		assertEquals(false, vp.isPubIDValid(5001));
		assertEquals(false, vp.isPubNameValid("abcdefghijabcdefghijabcdefghijA"));
		assertEquals(false, vp.isPubPriceValid("1001"));
		assertEquals(false, vp.isPubStockValid(10001));
	}

}
